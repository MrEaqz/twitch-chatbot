require('dotenv').config();
const tmi = require('tmi.js');
const client = new tmi.Client({
	options: { debug: true, messagesLogLevel: "info" },
	connection: {
		reconnect: true,
		secure: true
	},
	identity: {
		username: process.env.TWITCH_USERNAME,
		password: process.env.TWITCH_PASSWORD
	},
	channels: [ 'mr_eaqz' ]
});
client.connect().catch(console.error);

// Say hello to the tchat when i am connected to the channel
client.on("join", (channel, username, self) => {
	if (username == "mreaqz_bot") {
		client.say(channel, 'Bonjour le tchat Mau5 ! Je termine de paramétrer mes octets, mais je suis presque opérationnel !');
	}
});

// Say something when somone ping me
client.on('message', (channel, tags, message, self) => {
	if(self) return;
	if(message.includes("@mreaqz_bot")) {
		client.say(channel, `@${tags.username} Bonjour, je suis un robot crée par @mr_eaqz, je suis capable d'exécuter des commandes et d'alerter le chat de certains évenements. <3`);
	}
});

// When somone just sub to the channel
client.on("subscription", (channel, username, method, message, userstate) => {
	client.say(channel, `⭐️ Et c'est premier sub pour ${username} ✨ BIENVENUE ✨ !! ⭐️ `);
});


// User Resub
client.on("resub", (channel, username, months, message, userstate, methods) => {
	let cumulativeMonths = ~~userstate["msg-param-cumulative-months"];
	client.say(channel, `⭐️ Et c'est le ${cumulativeMonths} mois consécutif pour @${username} !! ⭐️ `);
});

// When somone gift a sub to an specific user
client.on("subgift", (channel, username, streakMonths, recipient, methods, userstate) => {
	let senderCount = ~~userstate["msg-param-sender-count"];
	client.say(channel, `⭐️ Et c'est ${username} qui offre son sub à ${recipient} (c'est son ${senderCount} sub offert) !! ⭐️ `);
});

// Is when somone continuing the Gift Sub they got from sender
client.on("giftpaidupgrade", (channel, username, sender, userstate) => {
	client.say(channel, `⭐️ Et c'est le resub de @${username} qui continue l'abonnement offert par @${sender} !! ⭐️ `);

});

// New Host incoming
client.on("hosted", (channel, username, viewers, autohost) => {
	client.say(channel, `💥 On se fait attaquer par @${username} qui viens avec ${viewers} envahisseurs !! 💥 `);

});

// When moderator clear chat
client.on("clearchat", (channel) => {
	client.say(channel, `First Kappa`);
});

// Own cmds
client.on('message', (channel, tags, message, self) => {
	if(self) return;
	if(message.toLowerCase() === "!control") {
		client.say(channel, `Je travaille actuellement sur un serveur ALT:V (GTA RP) pour plus d'informations sur le projet : https://discord.gg/r6vNFYTebG`);
	}
});

// Own cmds
client.on('message', (channel, tags, message, self) => {
	if(self) return;
	if(message.toLowerCase() === "!help") {
		client.say(channel, `Voici la liste des commandes disponibles : !help, !control`);
	}
});

console.log("[INFO] Everythings is OKAY")